import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  genders = [];
  gender: string;
  weight: number;
  time: number;
  bottles: number;
  promilles: number;

  constructor() {}

  ngOnInit() {
    this.genders.push('Female');
    this.genders.push('Male');

    this.gender = 'Female';
  }

  calculate() {

    const litres = this.bottles * 0.33;
    const grams = litres * 8 * 4.5;
    const burning = this.weight / 10;
    const grams_left = grams - (burning * this.time);

    if (this.gender === 'Male') {
      this.promilles = (grams_left / (this.weight * 0.7));
    } else {
      this.promilles = (grams_left / (this.weight * 0.6));
    }
  }

}
